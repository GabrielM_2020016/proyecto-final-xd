
let caracter={
  "type": "FeatureCollection",
  "features": [
       {
      "type": "Feature",
      "properties": {
"popupContent":"Mi planta uwu",
"tittle":"Caracteristicas generales",
"generaldescription":"Aquí encontrarás todo acerca del crecimiento y desarrollo de una planta de Luechuga, la cual aspira a convertirse en la hortaliza más grande que el mundo haya visto...",
"start":"2021/01/07(fecha de plantación)",
 
"descripcion1":"Para su proceso de plantación fue esencial una matera de tamaño considerable, al rededor de 7 semillas, dos bolsas de tierra y una manotada de abono",
 
"descripcion2":"Este especimen se encuentra entre el los 30 y los 31 días desde su plantación",
 
"descripcion3":"He aquí el proceso de plantación:",
 
"descripcion4":"Mi planta posee color verde tenue, necesita estar al aire libre y necesita de una roceada de agua diaria para seguir su desarrollo normal",
 
"descripcion5":"Mide aproximadamente 1.5 cm y consta de un tallo y tres pequeñas hojas",

"descripcion6":"En la actualidad vive, y vive de muy buena forma :), esta ubicada en mi casa y con los cuidados necesarios (los cuales no son muchos), llegará a extender su vida por mucho tiempo,"
 
      },
      "geometry": {
          "type": "Polygon",
          "coordinates": [
            [
              [
        4.639302577148279,
        -74.14351254701614
      ],
      [
        4.639392136749708,
        -74.14345823228359
      ],
      [
        4.639400825367156,
        -74.14347432553768
      ],
      [
        4.640150719485271,
        -74.143036454916
      ],
      [
        4.640164754929084,
        -74.14305623620749
      ],
      [
        4.640481220695302,
        -74.14287485182285
      ],
      [
        4.640463175132717,
        -74.1428406536579
      ],
      [
        4.640647975038787,
        -74.14272733032703
      ],
      [
        4.640633939604579,
        -74.14269682019949
      ],
      [
        4.641242475674272,
        -74.142383672297
      ],
      [
        4.64120972635494,
        -74.14232332259417
      ],
      [
        4.641663872392539,
        -74.14207085967064
      ],
      [
        4.64166988756989,
        -74.14208360016346
      ],
      [
        4.641735386164318,
        -74.14205007255077
      ],
      [
        4.641728702634557,
        -74.14203733205795
      ],
      [
        4.641777492400392,
        -74.1420091688633
      ],
      [
        4.6417881860472585,
        -74.14202593266963
      ],
      [
        4.641849006160671,
        -74.1419917345047
      ],
      [
        4.64184499604345,
        -74.14198100566864
      ],
      [
        4.641933218616725,
        -74.14193205535412
      ],
      [
        4.641918514855284,
        -74.14190791547298
      ],
      [
        4.642447181722045,
        -74.14159946143627
      ],
      [
        4.6426707455307215,
        -74.14187002927063
      ],
      [
        4.642478928455544,
        -74.14202023297548
      ],
      [
        4.6424959714381995,
        -74.142045378685
      ],
      [
        4.642076246098453,
        -74.14234779775141
      ],
      [
        4.642056195518662,
        -74.1423226520419
      ],
      [
        4.641356429926629,
        -74.14286378771067
      ],
      [
        4.641797208811656,
        -74.14347365498543
      ],
      [
        4.64160973579413,
        -74.14360139518975
      ],
      [
        4.6415840041996,
        -74.14356920868158
      ],
      [
        4.641574981432466,
        -74.14357658475637
      ],
      [
        4.641101453084282,
        -74.14293922483921
      ],
      [
        4.640469858674475,
        -74.14333920925856
      ],
      [
        4.640474871330736,
        -74.1433472558856
      ],
      [
        4.64036359035315,
        -74.14344917982817
      ],
      [
        4.639981959960413,
        -74.14373315870762
      ],
      [
        4.639963914385067,
        -74.14371337741612
      ],
      [
        4.639612025573679,
        -74.14394740015268
      ],
      [
        4.639302577148279,
        -74.14351254701614
      ]
            ]
          ]
        },
        
  }
  ]
}



let miGeoJSON=L.geoJSON(caracter);
miGeoJSON.addTo(miMapa);



