// Crear un objeto (variables compuestas) para representar el visor

//L= representa a la biblioteca leaflet}
//Creamos un objeto para representar nuestro visor 
let miMapa= L.map('mapid');

miMapa.setView([4.639710,-74.143560], 15);

let miProvedor=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

miProvedor.addTo(miMapa);

var greenIcon = L.icon({
  iconUrl: 'https://www.megaidea.net/wp-content/uploads/2020/03/plantas-vs-zombies-5-1024x851.png',


  iconSize:     [55, 75], // size of the icon
  shadowSize:   [50, 64], // size of the shadow
  iconAnchor:   [39, 79], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62],  // the same for the shadow
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var polygon = L.polygon([
  [
    4.639302577148279,
    -74.14351254701614
  ],
  [
    4.639392136749708,
    -74.14345823228359
  ],
  [
    4.639400825367156,
    -74.14347432553768
  ],
  [
    4.640150719485271,
    -74.143036454916
  ],
  [
    4.640164754929084,
    -74.14305623620749
  ],
  [
    4.640481220695302,
    -74.14287485182285
  ],
  [
    4.640463175132717,
    -74.1428406536579
  ],
  [
    4.640647975038787,
    -74.14272733032703
  ],
  [
    4.640633939604579,
    -74.14269682019949
  ],
  [
    4.641242475674272,
    -74.142383672297
  ],
  [
    4.64120972635494,
    -74.14232332259417
  ],
  [
    4.641663872392539,
    -74.14207085967064
  ],
  [
    4.64166988756989,
    -74.14208360016346
  ],
  [
    4.641735386164318,
    -74.14205007255077
  ],
  [
    4.641728702634557,
    -74.14203733205795
  ],
  [
    4.641777492400392,
    -74.1420091688633
  ],
  [
    4.6417881860472585,
    -74.14202593266963
  ],
  [
    4.641849006160671,
    -74.1419917345047
  ],
  [
    4.64184499604345,
    -74.14198100566864
  ],
  [
    4.641933218616725,
    -74.14193205535412
  ],
  [
    4.641918514855284,
    -74.14190791547298
  ],
  [
    4.642447181722045,
    -74.14159946143627
  ],
  [
    4.6426707455307215,
    -74.14187002927063
  ],
  [
    4.642478928455544,
    -74.14202023297548
  ],
  [
    4.6424959714381995,
    -74.142045378685
  ],
  [
    4.642076246098453,
    -74.14234779775141
  ],
  [
    4.642056195518662,
    -74.1423226520419
  ],
  [
    4.641356429926629,
    -74.14286378771067
  ],
  [
    4.641797208811656,
    -74.14347365498543
  ],
  [
    4.64160973579413,
    -74.14360139518975
  ],
  [
    4.6415840041996,
    -74.14356920868158
  ],
  [
    4.641574981432466,
    -74.14357658475637
  ],
  [
    4.641101453084282,
    -74.14293922483921
  ],
  [
    4.640469858674475,
    -74.14333920925856
  ],
  [
    4.640474871330736,
    -74.1433472558856
  ],
  [
    4.64036359035315,
    -74.14344917982817
  ],
  [
    4.639981959960413,
    -74.14373315870762
  ],
  [
    4.639963914385067,
    -74.14371337741612
  ],
  [
    4.639612025573679,
    -74.14394740015268
  ],
  [
    4.639302577148279,
    -74.14351254701614
  ]

]).addTo(miMapa);

let miMarcador = L.marker([4.640307963168979,-74.14314916286276], {icon: greenIcon}).addTo(miMapa);

miMarcador.bindPopup("He aquí el lugar de plantación.").openPopup();
miMarcador.addTo(miMapa);

let Encabezado=document.getElementById("titulo");
Encabezado.textContent=caracter.features[0].properties.popupContent
let miTitulo2=document.getElementById("tittle");
miTitulo2.textContent=caracter.features[0].properties.tittle
let miDescripciongeneral=document.getElementById("generaldescription");
miDescripciongeneral.textContent=caracter.features[0].properties.generaldescription
let inicio=document.getElementById("start");
inicio.textContent=caracter.features[0].properties.start
let miDescripcion1=document.getElementById("description1");
miDescripcion1.textContent=caracter.features[0].properties.descripcion1
let miDescripcion2=document.getElementById("description2");
miDescripcion2.textContent=caracter.features[0].properties.descripcion2
let miDescripcion3=document.getElementById("description3");
miDescripcion3.textContent=caracter.features[0].properties.descripcion3
let miDescripcion4=document.getElementById("description4");
miDescripcion4.textContent=caracter.features[0].properties.descripcion4
let miDescripcion5=document.getElementById("description5");
miDescripcion5.textContent=caracter.features[0].properties.descripcion5
let miDescripcion6=document.getElementById("description6");
miDescripcion6.textContent=caracter.features[0].properties.descripcion6



//JSON
let circle = L.circle([4.639710,-74.143560], {
    color: 'red',
    fillColor: 'red',
    fillOpacity: 0.5,
    radius: 645
});

circle.addTo(miMapa);
